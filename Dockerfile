FROM python:3.9-buster
MAINTAINER István Rábel <thraex.aquator@icloud.com>

# To compile Python Cryptography on ARMv7 Rust is needed.
RUN curl https://sh.rustup.rs -sSf | bash -s -- -y

COPY . /opt/wol
WORKDIR /opt/wol
RUN export PATH="/root/.cargo/bin/:$PATH" \
 && pip install -q .

ENTRYPOINT ["pyhapwol"]
