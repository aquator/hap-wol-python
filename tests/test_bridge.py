from pyhapwol.bridge import start, _validate_config
from pyhapwol import __version__
from sys import stderr
from unittest import TestCase, main
from unittest.mock import patch


class BridgeTest(TestCase):

    @patch('builtins.print')
    def test_validate_config(self, mock_print):
        self.assertFalse(_validate_config([]), 'Empty config is not valid')
        mock_print.assert_not_called()

        self.assertFalse(_validate_config([{}]), 'Name is missing')
        mock_print.assert_called_with('name is missing\n', file=stderr)

        self.assertFalse(_validate_config([{
            'name': 'Foo'
        }]), 'MAC is missing')
        mock_print.assert_called_with('mac is missing\n', file=stderr)

        self.assertFalse(_validate_config([{
            'name': 'Foo',
            'mac': 'bar'
        }]), 'MAC is invalid')
        mock_print.assert_called_with('bad mac format\n', file=stderr)

        self.assertFalse(_validate_config([{
            'name': 'Foo',
            'mac': '00:00:00:00:00:00'
        }]), 'Broadcast Address is missing')
        mock_print.assert_called_with('broadcast is missing\n', file=stderr)

        self.assertFalse(_validate_config([{
            'name': 'Foo',
            'mac': '00:00:00:00:00:00',
            'broadcast': 'baz'
        }]), 'Broadcast Address is invalid')
        mock_print.assert_called_with('bad broadcast format\n', file=stderr)

        mock_print.reset_mock()
        self.assertTrue(_validate_config([{
            'name': 'Foo',
            'mac': '00:00:00:00:00:00',
            'broadcast': '10.0.0.0/24'
        }]), 'Minimal valid configuration')
        mock_print.assert_not_called()

        self.assertFalse(_validate_config([{
            'name': 'Foo',
            'mac': '00:00:00:00:00:00',
            'broadcast': '10.0.0.0/24',
            'ip': 'baz'
        }]), 'IP is invalid')
        mock_print.assert_called_with('bad ip format\n', file=stderr)

        self.assertFalse(_validate_config([{
            'name': 'Foo',
            'mac': '00:00:00:00:00:00',
            'broadcast': '10.0.0.0/24',
            'port': 'baz'
        }]), 'Port is invalid')
        mock_print.assert_called_with('bad port format\n', file=stderr)

        mock_print.reset_mock()
        self.assertTrue(_validate_config([
            {
                'name': 'Foo',
                'mac': '00:00:00:00:00:00',
                'broadcast': '10.0.0.0/24'
            },
            {
                'name': 'Bar',
                'mac': '00:00:00:00:00:01',
                'broadcast': '10.0.0.1/24',
                'ip': '10.0.0.2',
                'port': '9',
                'interface': 'wlan0'
            }
        ]), 'Multiple valid entries')
        mock_print.assert_not_called()

    @patch('pyhapwol.bridge.AccessoryDriver')
    @patch('pyhapwol.bridge.Bridge')
    @patch('pyhapwol.bridge.Switch')
    def test_start(self, MockSwitch, MockBridge, MockDriver):
        with patch('builtins.print') as mock_print:
            with self.assertRaises(SystemExit) as ex:
                start([], None)
            self.assertEqual(ex.exception.code, 1, 'exits with code 1')
            mock_print.assert_called_with('Invalid configuration.\n',
                                          file=stderr)

        mock_driver = MockDriver.return_value
        mock_bridge = MockBridge.return_value
        mock_switch = MockSwitch.return_value

        config = [{
            'name': 'Foo',
            'mac': '00:00:00:00:00:00',
            'broadcast': '10.0.0.0/24'
        }]
        start(config, 'statefile')
        MockDriver.assert_called_with(port=51826, persist_file='statefile')
        MockBridge.assert_called_with(mock_driver, 'Bridge')
        MockSwitch.assert_called_with(config[0], mock_driver, 'Foo')
        mock_bridge.set_info_service. \
            assert_called_with(__version__, 'RabelCraft', 'PyHAP Bridge',
                               'BR-WoL-01')
        mock_bridge.add_accessory. \
            assert_called_with(mock_switch)
        mock_driver.add_accessory.assert_called_with(accessory=mock_bridge)
        mock_driver.start.assert_called_once()


if __name__ == '__main__':
    main()
