from pyhapwol.network import get_ip, ping
from unittest import TestCase, main
from unittest.mock import patch, MagicMock


class NetworkTest(TestCase):

    @patch('pyhapwol.network.Ether')
    @patch('pyhapwol.network.ARP')
    @patch('pyhapwol.network.srp1')
    def test_get_ip(self, mock_srp1, MockARP, MockEther):
        mac = 'a0:b1:c2:d3:e4:f5'
        broadcast = '10.0.0.0/24'
        interface = 'eth0'
        ip = '10.0.0.2'

        ether = MockEther.return_value
        arp = MockARP.return_value

        mock_srp1.return_value = None
        self.assertIsNone(get_ip(mac, broadcast, interface), 'Not found')

        MockEther.assert_called_with(dst=mac)
        MockARP.assert_called_with(pdst=broadcast)
        mock_srp1.assert_called_with(ether/arp, iface=interface, timeout=2,
                                     verbose=0)

        mock_srp1.return_value = {MockARP: MagicMock()}
        mock_srp1.return_value[MockARP].hwsrc = '00:00:00:00:00:00'
        self.assertIsNone(get_ip(mac, broadcast, interface), 'Not matching')

        MockEther.assert_called_with(dst=mac)
        MockARP.assert_called_with(pdst=broadcast)
        mock_srp1.assert_called_with(ether/arp, iface=interface, timeout=2,
                                     verbose=0)

        mock_srp1.return_value[MockARP].hwsrc = mac
        mock_srp1.return_value[MockARP].psrc = ip
        self.assertEqual(get_ip(mac, broadcast, interface), ip, 'Matching')

        MockEther.assert_called_with(dst=mac)
        MockARP.assert_called_with(pdst=broadcast)
        mock_srp1.assert_called_with(ether/arp, iface=interface, timeout=2,
                                     verbose=0)

    @patch('pyhapwol.network.IP')
    @patch('pyhapwol.network.ICMP')
    @patch('pyhapwol.network.sr1')
    def test_ping(self, mock_sr1, MockICMP, MockIP):
        interface = 'eth0'
        ip = '10.0.0.2'

        ip = MockIP.return_value
        icmp = MockICMP.return_value

        mock_sr1.return_value = None
        self.assertFalse(ping(ip, iface=interface), 'Not found')
        mock_sr1.assert_called_with(ip/icmp, iface=interface, timeout=2,
                                    verbose=0)
        MockIP.assert_called_with(dst=ip)
        MockICMP.assert_called_with()

        mock_sr1.return_value = {MockIP: MagicMock()}
        mock_sr1.return_value[MockIP].src = '10.0.0.1'
        self.assertFalse(ping(ip, iface=interface), 'Not matching')
        mock_sr1.assert_called_with(ip/icmp, iface=interface, timeout=2,
                                    verbose=0)
        MockIP.assert_called_with(dst=ip)
        MockICMP.assert_called_with()

        mock_sr1.return_value = {MockIP: MagicMock()}
        mock_sr1.return_value[MockIP].src = ip
        self.assertTrue(ping(ip, iface=interface), 'Matching')
        mock_sr1.assert_called_with(ip/icmp, iface=interface, timeout=2,
                                    verbose=0)
        MockIP.assert_called_with(dst=ip)
        MockICMP.assert_called_with()


if __name__ == '__main__':
    main()
