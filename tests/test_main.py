from click.testing import CliRunner
from logging import DEBUG, INFO
from pyhapwol import __version__
from pyhapwol.__main__ import main
from unittest import TestCase, main as test_main
from unittest.mock import patch
from yaml.loader import SafeLoader


class MainTest(TestCase):
    @patch('pyhapwol.__main__.basicConfig')
    @patch('builtins.open')
    @patch('pyhapwol.__main__.load')
    @patch('pyhapwol.__main__.start')
    def test_main(self, mock_start, mock_load, mock_open, mock_basicConfig):
        runner = CliRunner()
        config_file = mock_open.return_value.__enter__()
        config = mock_load.return_value

        result = runner.invoke(main, '--help')
        assert 'Start HomeKit Server with '\
               'specified configuration and state file.' in result.output
        mock_start.assert_not_called()

        result = runner.invoke(main, '--version')
        self.assertEqual(result.output, f'pyhapwol, version {__version__}\n')
        mock_start.assert_not_called()

        result = runner.invoke(main, '--debug')
        mock_basicConfig.assert_called_with(level=DEBUG)
        mock_open.assert_called_with('config.yaml', 'r')
        mock_load.assert_called_with(config_file, Loader=SafeLoader)
        mock_start.assert_called_with(config, 'homekit.state')

        result = runner.invoke(main, '--config test-config --state test-state')
        mock_basicConfig.assert_called_with(level=INFO)
        mock_open.assert_called_with('test-config', 'r')
        mock_load.assert_called_with(config_file, Loader=SafeLoader)
        mock_start.assert_called_with(config, 'test-state')


if __name__ == '__main__':
    test_main()
