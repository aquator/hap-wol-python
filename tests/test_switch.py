from pyhap.accessory import Accessory
from pyhapwol.switch import Switch
from pyhapwol import __version__
from unittest import TestCase, main
from unittest.mock import patch, MagicMock


class SwitchTest(TestCase):
    def test_init(self):
        switch = self._create_switch()
        self.assertIsInstance(switch, Accessory, 'Switch is Accessory')

        information_service = switch.get_service('AccessoryInformation')
        information_service.configure_char\
            .assert_any_call('FirmwareRevision', value=str(__version__)),
        information_service.configure_char\
            .assert_any_call('Manufacturer', value='RabelCraft'),
        information_service.configure_char\
            .assert_any_call('Model', value='PyHAP WoL Switch'),
        information_service.configure_char\
            .assert_any_call('SerialNumber', value='WoL#00:11:22:33:44:55')

        switch_service = switch.get_service('Switch')
        switch_service.configure_char\
            .assert_called_with('On', setter_callback=switch._set_on)

    @patch('pyhapwol.switch.wake')
    @patch('pyhapwol.switch.ping')
    @patch('pyhapwol.switch.get_ip')
    def test_set_on_callback(self, get_ip, ping, wake):
        ping.return_value = True
        get_ip.return_value = '10.0.0.10'
        switch = self._create_switch({
                'name': 'Test Switch',
                'mac': 'AA:BB:CC:DD:EE:FF',
                'broadcast': '10.0.0.0/24'
            })

        # Not waking if pinging
        switch._set_on(1)
        wake.assert_not_called()

        # Waking if not pinging
        ping.return_value = False
        switch._set_on(1)
        wake.assert_called_with('aa:bb:cc:dd:ee:ff', ip_address='10.0.0.0/24',
                                port=9, return_dest=True)
        self.assertEqual(switch._ip_address, '10.0.0.10', 'IP address saved')

        switch._set_on(0)
        self.assertIsNone(switch._ip_address, 'IP address cleared')

        wake.reset_mock()
        get_ip.return_value = None
        # Waking if no ip
        switch._set_on(1)
        wake.assert_called_with('aa:bb:cc:dd:ee:ff', ip_address='10.0.0.0/24',
                                port=9, return_dest=True)

    def _create_switch(self, config=None):
        if config is None:
            config = {
                'name': 'Test Switch',
                'mac': '00:11:22:33:44:55',
                'broadcast': '10.0.0.0/24'
            }
        mock_driver = MagicMock()
        mock_loader = mock_driver.loader

        mock_services = {
            'AccessoryInformation': MagicMock(),
            'Switch': MagicMock()
        }
        for name, value in mock_services.items():
            value.display_name = name

        mock_loader.get_service.side_effect = mock_services.get

        return Switch(config, mock_driver, 'Test Switch')


if __name__ == '__main__':
    main()
